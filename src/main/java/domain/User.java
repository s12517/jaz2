package domain;

public class User {
	
	private String username;
	private String password;
	private String email;
	private boolean AccessToPremium;
	private boolean profileAccess;
	private boolean canGivePermission;
	private int id;
	
	
	public User(){}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isAccessToPremium() {
		return AccessToPremium;
	}

	public void setAccessToPremium(boolean accessToPremium) {
		AccessToPremium = accessToPremium;
	}

	public boolean isProfileAccess() {
		return profileAccess;
	}

	public void setProfileAccess(boolean profileAccess) {
		this.profileAccess = profileAccess;
	}

	public boolean isCanGivePermission() {
		return canGivePermission;
	}

	public void setCanGivePermission(boolean canGivePermission) {
		this.canGivePermission = canGivePermission;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	

}
