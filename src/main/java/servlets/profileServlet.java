package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.User;
import repositories.DummyUserRepository;

@WebServlet("/profile")
	
	public class profileServlet extends HttpServlet {
	
		private static final long serialVersionUID = 1L;
		
		@Override 
		protected void doGet(HttpServletRequest request,HttpServletResponse response) 
		throws IOException,ServletException{
		    this.doPost(request,response);
		}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,  IOException{
			
		HttpSession session = request.getSession();
		User user = retriveApplicationFromRequest(request);
		List<User> repository = DummyUserRepository.getMyList();
		
		response.setContentType("text/html");
		if(request.getSession().getAttribute("log") == null){
		    request.getSession().setAttribute("log", "anonymous");
		    } 
			
		for(int i = 0;i<repository.size();i++){
		if(user.getUsername().equals(repository.get(i).getUsername())){
			response.getWriter().println("<h1>Twoj profil</h1>"+
					"Twoj nik:" +
					repository.get(i).getUsername()+"</br>"+
					"Twoj adres e-mail:"+
					repository.get(i).getEmail()+"</br>"+
					"Twoje haslo:"+
					repository.get(i).getPassword()+"</br>"+
					"Prawda, ze masz premium? :" +
					repository.get(i).isAccessToPremium()+"</br>"
					); 
			if(repository.get(i).isAccessToPremium()==true){
				response.getWriter().println(
						"<a href='http://localhost:8080/premium.jsp'>Premium content</a></br>");

		}
			if(repository.get(i).isCanGivePermission()==true){
				response.getWriter().println(
						"<a href='http://localhost:8080/allusers'>Wszyscy uzytkownicy</a></br>");
				response.getWriter().println(
						"<a href='http://localhost:8080/permissions.jsp'>Zmien komus uprawnienia tym formularzem</a>");
		}
		}
		}
	}

	private User retriveApplicationFromRequest (HttpServletRequest request){
		User result = new User();
		result.setUsername(request.getParameter("username"));

		return result;
	}
}
