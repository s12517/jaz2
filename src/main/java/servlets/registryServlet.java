package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import repositories.DummyUserRepository;
import repositories.UserRepository;
import domain.User;


@WebServlet("/add")
public class registryServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	UserRepository repository;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		User user = retriveUserFromRequest(request);
		repository = new DummyUserRepository();
			
		session.setAttribute("conf", user);
		
		repository.add(user);
		response.sendRedirect("/sukces.jsp");
	}
  	
	private User retriveUserFromRequest (HttpServletRequest request){
		User result = new User();
	//	if ((request.getParameter("password")).equals(request.getParameter("confirm"))){
		result.setUsername(request.getParameter("username"));
		result.setPassword(request.getParameter("password"));
		result.setEmail(request.getParameter("email"));
		result.setAccessToPremium(false);
		result.setCanGivePermission(false);
		result.setProfileAccess(true);
		
	//	}
		return result;
}
}

