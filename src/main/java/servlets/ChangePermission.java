package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.User;
import repositories.DummyUserRepository;

@WebServlet("/changepermission")

public class ChangePermission extends HttpServlet {
	
	
	private static final long serialVersionUID = 1L;
	
	   protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        doPost(request,response);
	    }

public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,  IOException{
		
	HttpSession session = request.getSession();
	User user = retriveApplicationFromRequest(request);
	List<User> repository = DummyUserRepository.getMyList();
	
	
		response.setContentType("text/html");
		
	
	

		for(int i = 0;i<repository.size();i++){
			if(user.getUsername().equals(repository.get(i).getUsername())){
		if(repository.get(i).isAccessToPremium()==true){
				repository.get(i).setAccessToPremium(false);
				response.getWriter().println("Uzytkowik stracil uprawnienia do konta premium");
		}else{
			repository.get(i).setAccessToPremium(true);
			response.getWriter().println("Uzytkowik nabyl uprawnienia do konta premium");
		
		}
	}
	}
	}


private User retriveApplicationFromRequest (HttpServletRequest request){
	User result = new User();
	result.setUsername(request.getParameter("username"));

	return result;
}

	
}
