package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.User;
import repositories.DummyUserRepository;
import repositories.UserRepository;

@WebServlet("/adder")
public class UserAdder  extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	UserRepository repository;
	
	@Override 
	protected void doGet(HttpServletRequest request,HttpServletResponse response) 
	throws IOException,ServletException{
	    this.doPost(request,response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
	repository = new DummyUserRepository();
	
	User admin = new User();
	admin.setUsername("admin");
	admin.setPassword("admin");
	admin.setEmail("admin@yourhome.pl");
	admin.setCanGivePermission(true);
	admin.setAccessToPremium(true);
	admin.setProfileAccess(true);
	
	repository.add(admin);
	
	User premium = new User();
	premium.setUsername("premium");
	premium.setPassword("premium");
	premium.setEmail("premium@yourhome.pl");
	premium.setCanGivePermission(false);
	premium.setAccessToPremium(true);
	premium.setProfileAccess(true);
	
	repository.add(premium);
	
	User normal= new User();
	normal.setUsername("normal");
	normal.setPassword("normal");
	normal.setEmail("normal@yourhome.pl");
	normal.setCanGivePermission(false);
	normal.setAccessToPremium(false);
	normal.setProfileAccess(true);
	
	repository.add(normal);
	
	response.sendRedirect("/index2.jsp");

}
}
