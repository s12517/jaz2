package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.User;
import repositories.DummyUserRepository;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	


public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
	HttpSession session = request.getSession();
	String permission = "anonymous";
	session.setAttribute("permission", permission);
	
	List<User> repository = DummyUserRepository.getMyList();
	response.setContentType("text/html");
	boolean foundUser = false;
		
	for(int i = 0;i<repository.size();i++){
		if(repository.get(i).getUsername().equals(request.getParameter("username"))){
			permission = "normal";
		response.getWriter().println("Znaleziono uzytkownika...");
		foundUser = true;
		}
		if(repository.get(i).getPassword().equals(request.getParameter("password"))){
			if(foundUser=true){
				response.sendRedirect("profile?username="+ repository.get(i).getUsername());
				response.getWriter().println("...i podano dobre haslo. Witaj " +repository.get(i).getUsername()+"!</br>");
			response.getWriter().println(
					"<a href='http://localhost:8080/profile'>Idz do swojego profilu</a>");
		
			if(repository.get(i).isAccessToPremium()==true){
				permission = "premiumuser";
			if(repository.get(i).isCanGivePermission()==true){
				permission = "admin";
			}
			
			}
			
	}
	}else{
		response.getWriter().println("...ale podano zle haslo.");
		response.getWriter().println(
				"<a href='http://localhost:8080/logowanie.jsp'>Try again</a>");
	}
}
	response.getWriter().println(session.getAttribute("permission"));
}

private User retriveUserFromRequest (HttpServletRequest request){
	User result = new User();
	result.setUsername(request.getParameter("name"));
	result.setEmail(request.getParameter("email"));
	result.setPassword(request.getParameter("password"));
	return result;
}
}



