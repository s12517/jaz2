package repositories;

import java.util.List;

import domain.User;

public interface UserRepository {

	void add(User user);
	int count();
	User getUserByEmailAddress(String email);
	List<User> ListOfUsers();

}
