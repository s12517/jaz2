package repositories;

import java.util.ArrayList;
import java.util.List;

import domain.User;

public class DummyUserRepository implements UserRepository{
	  
	 
	private static List<User> db = new ArrayList<User>();
	
	@Override
	public User getUserByEmailAddress (String email){
		for (User user: db){
			if (user.getEmail().equalsIgnoreCase(email))
				return user;
		}
		return null;
	}

	@Override
	public void add(User user){
		db.add(user);
	}
	
	@Override
	public int count(){
		return db.size();
	}

	@Override
	public List<User> ListOfUsers(){
		
		return db;
	}
	
	public static List<User> getMyList() {
		return db;
	}
}

